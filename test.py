import requests
from bs4 import BeautifulSoup
import unittest

class TestAirPollution(unittest.TestCase):
    
    def test_get_data(self):
        response = requests.get('http://apims.doe.gov.my/apims/hourly2.php')
        soup = BeautifulSoup(response.text)
        table = soup.find("table")
        data_set = []
        k = 0
        for tr in table.find_all("tr"):
            k = k + 1
            if k == 1 :
                continue
            j = 0
            record = []
            for td in tr.find_all("td"):
                if j == 0 or j==1:
                    record.append(td.string)
                else:
                    record.append(td.find("b").string)
                j = j + 1 
            if len(record) > 0:
                data_set.append(record)
        print data_set    


if __name__ == '__main__':
    unittest.main()