from apscheduler.schedulers.blocking import BlockingScheduler
import requests
from bs4 import BeautifulSoup
import pymysql


URL = 'http://apims.doe.gov.my/apims/hourly2.php)'

def job():
    content = get_content(URL)
    data_set = get_data(content)
    save_data_into_db(data_set)

def get_content(url):
    response = requests.get(url)
    return response.text

def get_data(content):
    soup = BeautifulSoup(content)
    table = soup.find("table")
    data_set = []
    if table is None:
        return data_set
    k = 0
    for tr in table.find_all("tr"):
        k = k + 1
        if k == 1 :
            continue
        j = 0
        record = []
        for td in tr.find_all("td"):
            if j == 0 or j==1:
                record.append(td.string)
            else:
                record.append(td.find("b").string)
            j = j + 1 
        if len(record) > 0:
            data_set.append(record)
    return data_set   
    
def save_data_into_db(data_set):
    if data_set:
        try:
            conn = pymysql.connect(host='localhost', port=3306, user='root', passwd='', db='weather')
            cur = conn.cursor()
            for data in data_set:
                sql = "insert into air_pollution (state,area,six_am,seven_am,eight_am,nine_am,ten_am,eleven_am ) value(%s,%s,%s,%s,%s,%s,%s,%s)"
                cur.execuet(sql,data)
        except Exception, e:
            print e
        finally:
            if cur is not None:
                cur.close()
            if conn is not None:
                conn.close()
             

if __name__ == '__main__':
    scheduler = BlockingScheduler()
    scheduler.add_job(job, 'interval', seconds=5)
    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        print "Exit the scrape work"
